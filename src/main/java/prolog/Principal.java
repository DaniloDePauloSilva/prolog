package prolog;

import java.util.List;
import java.util.Scanner;

public class Principal {
	
	public static void main(String[] args)
	{
		try 
		{
			//OBTEN��O DO CAMINHO DO ARQUIVO DE TEXTO QUE CONT�M A BASE DE CONHECIMENTO 
			String caminhoArquivo = args[0];
			
			Scanner sc = new Scanner(System.in);
			
			List<Predicado> listaPredicados = IO.retornaListaPredicadosDeArquivo(caminhoArquivo);
			
			System.out.println("digite a consulta, ou digite \"sair\" para fechar o programa.\n\n");
			while(true)
			{
				System.out.print("?- ");
				String str = sc.nextLine();
				
				if(str.equalsIgnoreCase("sair"))
				{
					sc.close();
					System.out.println("TERMINOU");
					System.exit(0);
				}
				
				try 
				{
					boolean unificavel = false;
					
					for(Predicado p : listaPredicados)
					{
						Predicado pIn = AnalisadorSentencaProlog.retornaPredicado(str);
						Lista lIn = Lista.transformaPredicadoEmLista(pIn);	
						
						Lista l = Lista.transformaPredicadoEmLista(p);
						
						Composicao c = (Composicao) Lista.unificar(lIn, l);
						
						if(c != null)
						{
							if(!unificavel)
							{
								System.out.println("UNIFIC�VEL");
								unificavel = true;
							}
							
							System.out.println(p.stringPredicado());
							System.out.println("Composi��o: " + c);
						}
					}
					
					if(!unificavel)
					{
						System.out.println("N�O UNIFIC�VEL");
					}
				}
				catch(Exception ex)
				{
					System.out.println("CONSULTA INV�LIDA: " + ex.getMessage());
				}
				
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
