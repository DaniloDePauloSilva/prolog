package prolog;

public class Constante extends Termo implements ItemLista{

	public Constante(String descricao) {
		
		super.setDescricao(descricao);
		
	}

//	public String toString() 
//	{
//		StringBuilder str = new StringBuilder();
//	
//		str.append("[simbolo");
////		str.append("[simbolo: ");
////		str.append(super.getDescricao());
//		str.append("]");
//		
//		return str.toString();
//	}
//
	
	public String toString() 
	{
		if(getDescricao() == null)
			setDescricao("");
		
		return getDescricao();
	}
	
	public boolean equals(Object obj) 
	{
		if(obj instanceof Constante)
		{
			Constante s = (Constante) obj;
			
			return this.getDescricao().equals(s.getDescricao());
		}
		
		return false;
	}
}
