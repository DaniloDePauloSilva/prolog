package prolog;

import java.util.ArrayList;
import java.util.List;

public class Predicado implements ItemLista {
	
	private String descricao;
	private List<Termo> listaTermos;
	
	public Predicado(String descricao) {
		
		setDescricao(descricao);
	}
	
	public Predicado(String descricao, List<Termo> listaTermos) {
		
		this.setDescricao(descricao);
		this.setListaTermos(listaTermos);
		
	}

	public void setListaTermos(List<Termo> listaTermos) 
	{
		this.listaTermos = listaTermos;
	}

	public List<Termo> getListaTermos() 
	{
		if(listaTermos == null)
			listaTermos = new ArrayList<Termo>();
		
		return listaTermos;
	}
	
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	
	public String getDescricao() 
	{
		return this.descricao;
	}
	
//	@Override
//	public String toString() {
//		
//		StringBuilder str = new StringBuilder();
//		
////		str.append("[PREDICADO: " + this.getDescricao());
////		str.append(", TERMOS: [");
//		
//		str.append("[PREDICADO");
//		str.append(", TERMOS: [");
//		
//		for(int i = 0; i < getListaTermos().size(); i++)
//		{
//			str.append(getListaTermos().get(i).toString());
//	
//			if(i != getListaTermos().size() - 1)
//			{
//				str.append(", ");
//			}
//		}
//		
//		str.append("]]");
//		
//		
//		
//		return str.toString();
//	}
	
	public String stringPredicado() 
	{
		StringBuilder str = new StringBuilder();
		
		str.append(this.getDescricao());
		str.append("(");
		
		for(Termo t : getListaTermos())
		{
			str.append(t.toString());
			str.append(",");
		}
		
		str.deleteCharAt(str.length() - 1);
		
		str.append(")");
		
		return str.toString();
	}
	
	public String toString() 
	{
		if(this.descricao == null)
			descricao = "";
		
		return this.descricao;
	}

	
}
