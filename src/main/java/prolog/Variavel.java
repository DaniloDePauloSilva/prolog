package prolog;

public class Variavel extends Termo implements ItemLista {

	public Variavel(String descricao) {
	
		super.setDescricao(descricao);
	}

//	public String toString() {
//
//		StringBuilder str = new StringBuilder();
//		
//		str.append("[variavel");
////		str.append("[variavel: ");
////		str.append(super.getDescricao());
//		str.append("]");
//		
//		return str.toString();
//	}
	
	public String toString() 
	{
		return getDescricao();
	}
	
	public boolean equals(Object obj) 
	{
		if(obj instanceof Variavel)
		{
			Variavel v = (Variavel) obj;
			
			return this.getDescricao().equals(v.getDescricao()); 
		}
			
		return false;
	}

}
