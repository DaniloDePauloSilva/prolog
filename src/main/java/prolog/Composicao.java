package prolog;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Composicao extends ItemComposicao{
	
	private Set<ItemComposicao> listaItensComposicao;

	public Set<ItemComposicao> getListaItensComposicao() {
		
		if(listaItensComposicao == null)
			listaItensComposicao = new HashSet<ItemComposicao>();
		
		return listaItensComposicao;
	}

	public void setListaItensComposicao(Set<ItemComposicao> listaItensComposicao) {
		this.listaItensComposicao = listaItensComposicao;
	}
	
	public String toString() 
	{
		StringBuilder str = new StringBuilder();
		
		str.append("{ ");
		
		int i = 0;
		
		for(ItemComposicao ic : getListaItensComposicao())
		{
			if(ic.getItemPosterior() != null && ic.getItemAnterior() != null)
			{
				str.append(ic.getItemPosterior());
				str.append("/");
				str.append(ic.getItemAnterior());
				
				if(i < getListaItensComposicao().size() - 1)
					str.append(", ");
			}
			
			i++;
		}
		
		str.append(" }");
		
		return str.toString();
	}
	
}
