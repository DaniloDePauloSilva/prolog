package prolog;

import java.util.Scanner;

public class ExecutaProlog {
	
	public static void main(String[] args) throws Exception 
	{
		Scanner sc = new Scanner(System.in);
		//VERIFICADOR SENTEN�A
		while(true) 
		{
			System.out.println("==========================================");
			System.out.print("1 ?- ");
			
			String s = sc.nextLine();
			
			Predicado p = AnalisadorSentencaProlog.retornaPredicado(s);
			
			System.out.println("VERIFICA SENTEN�A: " + p);
			System.out.println("STRING LIDA: " + s);
			
			Lista l1 = Lista.transformaPredicadoEmLista(p);
			
			System.out.println("LISTA PROCESSADA: ");
			System.out.println(l1);
			
			System.out.println("==========================================");
			System.out.print("2 ?- ");
			s = sc.nextLine();
			
			System.out.println("VERIFICA SENTEN�A: " + p);
			System.out.println("STRING LIDA: " + s);
			
			Predicado p2 = AnalisadorSentencaProlog.retornaPredicado(s);
			
			Lista l2 = Lista.transformaPredicadoEmLista(p2);
			
			System.out.println("LISTA PROCESSADA: ");
			System.out.println(l2);
			
			Composicao c = (Composicao) Lista.unificar(l1, l2, null);
			
			System.out.println("==========================================");
			System.out.println("COMPOSICAO: ");
			System.out.println(c);
		}
		
	}

}
