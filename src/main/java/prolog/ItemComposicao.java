package prolog;

public class ItemComposicao{
	
	private ItemLista itemAnterior;
	private ItemLista itemPosterior;
	
	public ItemComposicao() 
	{
		
	}
	
	public ItemComposicao(ItemLista itemPosterior, ItemLista itemAnterior)
	{
		setItemAnterior(itemAnterior);
		setItemPosterior(itemPosterior);
	}
	
	public ItemLista getItemAnterior() {
		return itemAnterior;
	}
	
	public void setItemAnterior(ItemLista itemAnterior) {
		this.itemAnterior = itemAnterior;
	}
	
	public ItemLista getItemPosterior() {
		return itemPosterior;
	}
	
	public void setItemPosterior(ItemLista itemPosterior) {
		this.itemPosterior = itemPosterior;
	}
	
	public String toString() 
	{
		if(this.getItemAnterior() == null || this.getItemPosterior() == null)
		{
			return "";
		}
			
		return this.getItemPosterior().toString() + "/" + this.getItemAnterior().toString();
	}
	
	public boolean equals(Object obj) 
	{
		if(obj instanceof ItemComposicao)
		{
			ItemComposicao ic = (ItemComposicao) obj;
			
			return ic.getItemPosterior().toString().equals(this.getItemPosterior().toString()) &&
					ic.getItemAnterior().toString().equals(this.getItemAnterior().toString());
		}
			
		return false;
	}

}
