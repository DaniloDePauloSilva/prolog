package prolog;

import java.util.ArrayList;
import java.util.List;

public class Lista extends ArrayList<ItemLista> implements ItemLista{

	public static ItemComposicao unificar(ItemLista i1, ItemLista i2) 
	{
		return unificar(i1, i2, null);
	}
	
	public static ItemComposicao unificar(ItemLista i1, ItemLista i2, Composicao comp)
	{
		if(comp == null)
			comp = new Composicao();
		
		if((itemEhConstante(i1) && itemEhConstante(i2)) || (itemEhListaVazia(i1) && itemEhListaVazia(i2)))
		{
			if(compararConstantes(i1,i2) || (itemEhListaVazia(i1) && itemEhListaVazia(i2))) 
			{
				return new ItemComposicao();
			}
			else
			{
				return null;
			}
		}
		else if(itemEhVariavel(i1)) 
		{
			if(i1.equals(i2))
			{
				return null;
			}
			else 
			{
				return new ItemComposicao(i2, i1);
			}
		}
		else if(itemEhVariavel(i2))
		{
			if(i1.equals(i2))
			{
				return null;
			}
			else 
			{
				return new ItemComposicao(i1, i2);
			}
		}
		else if(itemEhListaVazia(i1) || itemEhListaVazia(i2) || itemEhConstante(i1) || itemEhConstante(i2))
		{
			return null;
		}
		else 
		{
			ItemLista he1 = ((Lista) i1).remove(0);
			ItemLista he2 = ((Lista) i2).remove(0);
			
			ItemComposicao subs1 = unificar(he1, he2, comp);
			
			if(subs1 == null)
				return null;
			
			Lista te1 = aplicarItemComposicao(subs1, (Lista) i1);
			Lista te2 = aplicarItemComposicao(subs1, (Lista) i2);
			
			ItemComposicao subs2 = unificar(te1, te2, comp);
			
			if(subs2 == null)
				return null;
			
			comp.getListaItensComposicao().add(subs1);
			comp.getListaItensComposicao().add(subs2);
			
			return comp;
		}
	}
	
	public static Lista aplicarItemComposicao(ItemComposicao ic, Lista restoDaLista)
	{
		Lista listaRetorno = (Lista) restoDaLista.clone();
		
		for(int i = 0; i < listaRetorno.size(); i++)
		{
			if(itemEhLista(listaRetorno.get(i)))
			{
				aplicarItemComposicao(ic,(Lista) listaRetorno.get(i));
			}
			else if(listaRetorno.get(i).equals(ic.getItemAnterior()))
			{
				listaRetorno.set(i, ic.getItemPosterior());
			}
		}
		
		return listaRetorno;
	}
	
	public static boolean compararConstantes(ItemLista i1, ItemLista i2) 
	{
		if(i1 instanceof Constante && i2 instanceof Constante)
		{
			Constante s1 = (Constante) i1;
			Constante s2 = (Constante) i2;
			
			return s1.getDescricao().equals(s2.getDescricao());
		}
		if(i1 instanceof Predicado && i2 instanceof Predicado)
		{
			Predicado p1 = (Predicado) i1;
			Predicado p2 = (Predicado) i2;
			
			return p1.getDescricao().equals(p2.getDescricao());
		}
		if(i1 instanceof Funcao && i2 instanceof Funcao)
		{
			Funcao f1 = (Funcao) i1;
			Funcao f2 = (Funcao) i2;
			
			return f1.getDescricao().equals(f2.getDescricao());
		}
		
		return false;
	}
	
	public static boolean itemEhConstante(ItemLista i)
	{
		return i instanceof Constante || i instanceof Predicado || i instanceof Funcao;
	}
	
	public static boolean itemEhLista(ItemLista i)
	{
		return i instanceof Lista;
	}
	
	
	public static boolean itemEhListaVazia(ItemLista i)
	{
		if(itemEhLista(i))
		{
			Lista l = (Lista) i;
			
			return l.size() == 0;
		}
		
		return false;
	}
	
	public static boolean itemEhVariavel(ItemLista i) 
	{
		return i instanceof Variavel;
	}
	
	public static Lista transformaPredicadoEmLista(Predicado predicado) 
	{
		Lista lista = new Lista();
		
		lista.add(predicado);
		
		for(Termo t : predicado.getListaTermos())
		{
			if(t instanceof Constante || t instanceof Variavel)
			{
				lista.add(t);
			}
			else if(t instanceof Funcao)
			{
				lista.add(transformaFuncaoEmLista((Funcao) t));
			}
			else 
			{
				return null;
			}
		}
		
		return lista;
		
	}
	
	public static Lista transformaFuncaoEmLista(Funcao funcao)
	{
		Lista lista = new Lista();
		
		lista.add(funcao);
		
		for(Termo t : funcao.getListaTermos())
		{
			if(t instanceof Constante || t instanceof Variavel)
			{
				lista.add(t);
			}
			else if(t instanceof Funcao)
			{
				lista.add(transformaFuncaoEmLista((Funcao) t));
			}
			else 
			{
				return null;
			}
		}
		
		return lista;
	}
	
	public String toString() 
	{
		StringBuilder str = new StringBuilder();
		
		str.append("[ ");
		
		for(int i = 0; i < this.size(); i++)
		{
			str.append(this.get(i).toString());
			
			if(i != this.size() - 1)
				str.append(", ");
		}
		
		str.append(" ]");
		
		return str.toString();
	}
	
	public boolean equals(Object obj) 
	{
		if(obj instanceof Lista)
		{
			Lista l = (Lista) obj;
			
			if(itemEhListaVazia(this) && itemEhListaVazia(l))
			{
				return true;
			}
			else if(this.size() == l.size()) 
			{
				for(int i = 0; i < this.size(); i++)
				{
					if(!this.get(i).equals(l.get(i))) 
					{
						return false;
					}
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	
	
}
