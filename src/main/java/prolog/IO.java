package prolog;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public abstract class IO {

	public static List<Predicado> retornaListaPredicadosDeArquivo(String caminhoArquivo) throws Exception
	{
		List<Predicado> listaRetorno = new ArrayList<Predicado>();
		BufferedReader br = new BufferedReader(new FileReader(caminhoArquivo));
		String linha;
		
		int cont = 1;
		
		System.out.println("CARREGANDO ARQUIVO: \n");
		while((linha = br.readLine()) != null)
		{
			try 
			{
				System.out.println(cont + ":\t" + linha);
				Predicado p = AnalisadorSentencaProlog.retornaPredicado(linha);
				
				listaRetorno.add(p);
			}
			catch(ExceptionAnaliseSentenca ex)
			{
				throw new ExceptionAnaliseSentenca("Problema ao ler arquivo: linha " + cont + ": " + ex.getMessage());
			}
			
			cont++;
		}
		
		br.close();
		
		System.out.println();
		
		return listaRetorno;
		
	}
	
}
