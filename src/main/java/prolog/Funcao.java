package prolog;

import java.util.List;

public class Funcao extends Termo implements ItemLista{

	private List<Termo> listaTermos;
	
	public Funcao(String descricao, List<Termo> listaTermos) {
		
		super.setDescricao(descricao);
		setListaTermos(listaTermos);
	}

	public List<Termo> getListaTermos() {
		return listaTermos;
	}

	public void setListaTermos(List<Termo> listaTermos) {
		this.listaTermos = listaTermos;
	}
	
	
	
	

}
