package prolog;

import java.util.ArrayList;
import java.util.List;

public abstract class AnalisadorSentencaProlog {
	
	public static Predicado retornaPredicado(String str) throws Exception
	{
		Predicado predicado = null;
		
		StringBuilder strDescricaoPredicado = new StringBuilder();
		
		//VERIFICA SE A STRING DE ENTRADA TERMINA COM "."
		if(!str.endsWith(".")) 
		{
			throw new ExceptionAnaliseSentenca("cadeia n�o termina com .");
		}
		else 
		{
			//REMOVE O �LTIMO PONTO
			str = str.substring(0, str.length() - 1);
		}
		
		//VERIFICA��O DE CADA CARACTER QUE COMP�E A SENTEN�A:
		for(int i = 0; i < str.length(); i++)
		{
			int codChar = ((int)str.charAt(i)); 
			
			if(i == 0)
			{
				//VERIFICA SE N�O COME�A COM LETRA MIN�SCULA
				if(!letraMinuscula(codChar))
				{
					//CASO O PRIMEIRO CARACTER N�O SEJA UMA LETRA MIN�SCULA, SER� LAN�ADA A FALHA 
					throw new ExceptionAnaliseSentenca("A primeira letra do predicado deve ser uma letra min�scula");
				}
				
				//CONCATENA O CARACTER ATUAL AO BUFFER QUE REPRESENTA A DESCRI��O DO PREDICADO
				strDescricaoPredicado.append((char) codChar);
			}
			else 
			{
				//CASO O CARACTER ATUAL DO LA�O SEJA LETRA MAI�SCULA
				if(letraMaiuscula(codChar) || letraMinuscula(codChar) || numero(codChar))
				{
					strDescricaoPredicado.append((char) codChar);
					continue;
				}
				
				if(abreParenteses(codChar))
				{
					//procura fim de ariedade e verifica termos
					int fimFecha = obterIndiceUltimoParenteses(str);
					
					//O fato do m�todo obterIndiceUltimoParenteses retornar um valor igual ao tamanho da String
					//representa que n�o existe o fechamento dos parenteses
					// neste caso ser� lan�ada uma exce��o
					if(fimFecha == str.length())
					{
						throw new ExceptionAnaliseSentenca("fechamento de parenteses esperado.");
					}
					
					if(fimFecha != str.length() - 1)
					{
						throw new ExceptionAnaliseSentenca("senten�a n�o terminada com \").\"");
					}
					
					//obt�m a string entre o abre e fecha parenteses do predicado
					String strTermos = str.substring(i + 1, fimFecha);
					
					//transforma a string de termos em uma lista de objetos 
					List<Termo> listaTermos = verificarTermos(strTermos);
					
					if(listaTermos != null)
					{
						predicado = new Predicado(strDescricaoPredicado.toString(), listaTermos);
						
						return predicado;
					}
					else 
					{
						throw new ExceptionAnaliseSentenca("falha aou analisar os termos");
					}
					
				}
				else 
				{
					throw new ExceptionAnaliseSentenca("caractere '" + ((char) codChar) + "' inv�lido.");
				}
			}
		}
		
		return predicado;
	}
	
	public static Funcao retornaFuncao(String str) throws Exception 
	{
		Funcao funcao = null;
		
		StringBuilder strDescricaoFuncao = new StringBuilder();
		
		//VERIFICA SE ACABA COM ")"
		if(!str.endsWith(")")) 
		{
			return null;
		}
		
		//VERIFICA��O DE CADA CARACTER QUE COMP�E A FUN��O
		for(int i = 0; i < str.length(); i++)
		{
			int codChar = ((int)str.charAt(i)); 
		
			if(i == 0)
			{
				//VERIFICA SE N�O COME�A COM LETRA MIN�SCULA
				if(!letraMinuscula(codChar))
				{
					//caso a cadeia n�o inicie com letra min�scula, ela n�o representa uma fun��o
					return null;
				}
				
				strDescricaoFuncao.append((char) codChar);
			}
			else 
			{
				if(letraMaiuscula(codChar) || letraMinuscula(codChar) || numero(codChar))
				{
					strDescricaoFuncao.append((char) codChar);
					continue;
				}
				
				//an�lise do in�cio da ariedade da fun��o
				if(abreParenteses(codChar))
				{
					//procurar fim de ariedade e verificar termos
					
					int fimFecha = obterIndiceUltimoParenteses(str);
					
					//caso n�o exista o fechamento de parenteses, a cadeia n�o representar� uma fun��o
					if(fimFecha == str.length())
					{
						return null;
					}
					
					//separa��o do peda�o da cadeia que cont�m os termos
					String strTermos = str.substring(i + 1, fimFecha);
					
					//verifica��o da cadeia que representa os termos da fun��o
					List<Termo> listaTermos = verificarTermos(strTermos);
					
					//caso a verifica��o retorne uma lista, a cadeia de entrada ser� convertida em um objeto do tipo fun��o
					if(listaTermos != null)
					{
						funcao = new Funcao(strDescricaoFuncao.toString(), listaTermos);
						
						return funcao;
					}
					else 
					{
						return null;
					}
					
				}
				else 
				{
					return null;
				}
			}
		}
		
		return funcao;
	}
	
	
	private static int obterIndiceUltimoParenteses(String str) {

		int indiceDifFim = inverteString(str).indexOf(")") + 1;
		return str.length() - indiceDifFim;
	}

	public static String inverteString(String str) 
	{
		StringBuilder retorno = new StringBuilder();
			
		for(int i = str.length() - 1; i >= 0; i--)
		{
			retorno.append(str.charAt(i));
		}
		
		return retorno.toString();
	}
	
	//m�todo que realiza a verifica��o de uma cadeia de caracteres que representa os termos de um predicado ou fun��o
	public static List<Termo> verificarTermos(String strTermos) throws Exception
	{
		//declara��o da lista que armazena cada termo v�lido da cadeia
		List<Termo> listaRetorno = new ArrayList<Termo>();
		
		List<String> listaTermos = new ArrayList<String>();
		StringBuilder strBuffer = new StringBuilder();
		
		//vari�vel de controle do n�mero de parenteses abertos durante a an�lise
		int numParentesesAbertos = 0;
		
		//verifica��o de cada caracter da cadeia de entrada
		for(int i = 0; i < strTermos.length(); i++)
		{
			char caractere = strTermos.charAt(i);
			int codChar = (int) caractere;
			
			if(i == 0)
			{
				//caso a cadeia n�o inicie com uma letra mai�scula ou com uma letra min�scula, ser� retornado nulo, representando que
				//a ca
				if(!letraMinuscula(codChar) && !letraMaiuscula(codChar))
				{
					return null;
				}
				
				strBuffer.append(caractere);
				
				if(strTermos.length() == 1)
					listaTermos.add(strBuffer.toString());
			}
			else 
			{
				if(numParentesesAbertos == 0)
				{
					boolean continua = false;
					
					if(letraMinuscula(codChar)|| letraMaiuscula(codChar) || numero(codChar))
					{
						strBuffer.append(caractere);
						continua = true;
					}
					
					if(virgula(codChar) || (i == strTermos.length() - 1))
					{
						continua = true;
						listaTermos.add(strBuffer.toString());
						
						strBuffer = new StringBuilder();
					}
					
					if(continua)
					{
						continue;
					}
				}
				
				if(abreParenteses(codChar))
				{
					strBuffer.append(caractere);
					numParentesesAbertos++;
					continue;
				}
				
				if(fecharParenteses(codChar) && (numParentesesAbertos > 0))
				{
					strBuffer.append(caractere);
					numParentesesAbertos--;
					
					if(i == strTermos.length() - 1)
						listaTermos.add(strBuffer.toString());
					
					continue;
				}
				
				if(numParentesesAbertos > 0)
				{
					strBuffer.append(caractere);
					continue;
				}
				
				return null;
			}
		}		
		
		if(numParentesesAbertos > 0)
		{
			return null;
		}
		
		for(String s : listaTermos)
		{
			if(ehSimbolo(s))
			{
				Termo termoSimbolo = new Constante(s);
				
				listaRetorno.add(termoSimbolo);
				
				continue;
			}
			else if(ehVariavel(s))
			{
				Termo termoVariavel = new Variavel(s);
				
				listaRetorno.add(termoVariavel);
				
				continue;
			}
			else 
			{
				Funcao funcao = retornaFuncao(s);
				
				if(funcao != null)
				{
					listaRetorno.add(funcao);
					continue;
				}
				
				return null;
			}
		}
		
		return listaRetorno;
	}
	

	//VERIFICA SE UM C�DIGO DE CARACTER REPRESENTA UMA VIRGULA
	private static boolean virgula(int codChar) {
		
		return codChar == 44;
	}

	//VERIFICA SE UMA STRING REPRESENTA UMA VARIAVEL
	public static boolean ehVariavel(String s)
	{
		for(int i = 0; i < s.length(); i++)
		{
			int codChar = (int) s.charAt(i);
		
			if(i == 0)
			{
				if(!letraMaiuscula(codChar))
				{
					return false;
				}
			}
			else 
			{
				if(!letraMinuscula(codChar) && !letraMaiuscula(codChar) && !numero(codChar))
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	//VERIFICA SE UMA STRING REPRESENTA UM SIMBOLO
	public static boolean ehSimbolo(String s)
	{
		for(int i = 0; i < s.length(); i++)
		{
			int codChar = (int) s.charAt(i);
		
			if(i == 0 && !letraMinuscula(codChar) )
			{
				return false;
			}
			else 
			{
				if(!letraMinuscula(codChar) && !letraMaiuscula(codChar) && !numero(codChar))
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static boolean abreParenteses(int codChar) 
	{
		return codChar == 40;
	}
	
	public static boolean fecharParenteses(int codChar)
	{
		return codChar == 41;
	}
	
	public static boolean numero(int codChar)
	{
		return (codChar >= 48 && codChar <= 57);
	}
	
	public static boolean letraMaiuscula(int codChar) 
	{
		return (codChar >= 65 && codChar <= 90);
	}
	
	public static boolean letraMinuscula(int codChar)
	{
		return (codChar >= 97 && codChar <= 122);
	}

}
